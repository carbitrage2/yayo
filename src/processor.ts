import { MarketData, QueryRow, DataEntry } from "../types/data";
import 'dotenv';

async function prepareSendData(wsData: MarketData): Promise<DataEntry[]> {
    return new Promise(async resolve => {
        const dbData: QueryRow[] = JSON.parse(process.env.DATA as string);
        let results: DataEntry[] = [];
        for await (let market of wsData.markets) {
            for await (let entry of dbData) {
                if (market == entry.market && wsData.pair == entry.pair) {
                    results.push({
                        id: market,
                        label: market,
                        value: entry.price,
                        color: 'hsl(51, 70%, 50%)'
                    });
                }
            }
        }
        resolve(results);
    });
}

export { prepareSendData };