import { config as dotenvConfig } from 'dotenv';
import { prepareData } from './db';
import { startServer } from './server';

function startSocket() {
    dotenvConfig();
    prepareData();
    startServer();
}

startSocket();